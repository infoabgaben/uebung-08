//package gruppe01.info08;


public class TicTacToe {
	
	//a
	//Attribute
	private char[][] board;
	private char[] playerMarks;
	private int player;
	
	//Konstruktor
	public TicTacToe() {
		
		board = new char[3][3];
		playerMarks = new char[]{'x', 'o'};
		
		this.resetGame();
	}
	
	//b	
	private void resetGame() {
		for(int x = 0; x < 3; x++) {
			for(int y = 0; y < 3; y++) {
				board[x][y] = ' ';
			}
		}

		player = 0;
	}
	
	//c
	private boolean hasWon() {
		
		char current;
		if(player == 0)
			current = 'x';
		else
			current = 'o';
		
		for(int y = 0; y < 3; y++) {	//Reihen
			if(board[0][y] == current && board[1][y] == current && board[2][y] == current)
				return true;
		}
		
		for(int x = 0; x < 3; x++) {	//Spalten
			if(board[x][0] == current && board[x][1] == current && board[x][2] == current)
				return true;
		}
	
		if(board[0][0] == current && board[1][1] == current && board[2][2] == current) //Diagonale
			return true;
		
		if(board[2][0] == current && board[1][1] == current && board[0][2] == current) //Diagonale
			return true;
		
		return false;
	}
	
	//d
	public void play(int i, int j) {
		if(board[i][j] != ' ')
			return;
		
		char current;
		if(player == 0)
			current = 'x';
		else
			current = 'o';
		
		board[i][j] = current;
		this.printBoard(board);
		
		if(this.hasWon()) {
			this.resetGame();
			System.out.println("Spieler " + current + " hat gewonnen!");
		
		}else if (this.isBoardFull(board)) {
			this.resetGame();
			System.out.println("Unentschieden!");			
		
		}else {
			if(player == 0)
				player = 1;
			else
				player = 0;
		}
	}
	// ------------------------------------------------------------------------------------------------ //
	// gegebene Methoden                                                                                //
	// ------------------------------------------------------------------------------------------------ //
	
	// prueft ob das gesamte Spielfeld mit den Zeichen der Spieler gefuellt ist.
	// wenn ja gib true zurueck, andernfalls false
	private boolean isBoardFull(char[][] board) {
		boolean isFull = true;
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(board[i][j] == ' ') isFull = false;
			}
		}
		return isFull;
	}
	
	// gibt den aktuellen Zustand des Spielfeldes auf der Konsole aus.
	private void printBoard(char[][] board) {
		String fieldVis = "";
		// zeichne Spielfeld
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				fieldVis += " " + board[i][j] + " ";
				if(j != 2) fieldVis += "|";
			}
			fieldVis += "\n";
			if(i != 2) fieldVis += "-----------\n";
		}
		System.out.println(fieldVis);
	}
	
	// ------------------------------------------------------------------------------------------------ //
	// main                                                                                             //
	// ------------------------------------------------------------------------------------------------ //
	
	public static void main(String[] args) {
		TicTacToe ttt = new TicTacToe();
		
		// DER CODE KANN AUSKOMMENTIERT WERDEN SOBALD AUFGABE A BEARBEITET WURDE
		System.out.println("\n--- Spiel 1 ---\n");
		ttt.play(1, 1);
		ttt.play(0, 1);
		ttt.play(2, 0);
		ttt.play(0, 2);
		ttt.play(2, 2);
		ttt.play(2, 1);
		ttt.play(0, 0);
		
		System.out.println("\n--- Spiel 2 ---\n");
		ttt.play(0, 1);
		ttt.play(0, 0);
		ttt.play(2, 1);
		ttt.play(1, 1);
		ttt.play(0, 2);
		ttt.play(2, 1);
		ttt.play(1, 0);
		ttt.play(2, 0);
		ttt.play(1, 2);
		
		System.out.println("\n--- Spiel 3 ---\n");
		ttt.play(2, 2);
		ttt.play(0, 0);
		ttt.play(1, 2);
		ttt.play(0, 2);
		ttt.play(0, 1);
		ttt.play(1, 0);
		ttt.play(2, 0);
		ttt.play(2, 1);
		ttt.play(1, 1);
	}
}
